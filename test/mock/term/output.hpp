#pragma once

#include <term/output.hpp>

namespace {
    struct TermOut : public term::Output
    {
        TermOut() = default;
        
        TermOut& write( std::string_view ) { return *this; }
        
        TermOut& clear_screen() { return *this; }
        
        
        TermOut& set_color( term::Color const& ) { return *this; }
        
        TermOut& set_bg_color( term::Color const& ) { return *this; }
        
        TermOut& reset_mode() { return *this; }

        std::vector<term::Position> set_pos_;
        TermOut& set_position( term::Position p )
        {
            set_pos_.push_back( p );
            return *this; 
        }
        
        short get_width() const { return 100; }

        void flush() const {}
    };
}
