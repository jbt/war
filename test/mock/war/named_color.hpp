#pragma once

#include <war/named_color.hpp>
#include <map>

namespace {
    std::map<war::NamedColor,term::Color> test_colors;
}

auto war::get(NamedColor nc) -> term::Color
{ 
    return test_colors[nc];
}

void war::assign_rgb( NamedColor nc, rgb_int rgb_code )
{ 
    test_colors[nc] = term::Color{ rgb_code };
}
