#pragma once

#include <war/display.hpp>

#include <mock/war/named_color.hpp>
#include <term/color.hpp>
#include <term/utf8.hpp>

namespace {
    short test_term_width = 99;
}

war::Display::Display( std::array<Player,2> const& players, Slowness slowness, term::Output& term_out )
: players_{ players }
, slowness_{ slowness }
, term_{ term_out }
{
}

war::Display::~Display()
{
}

auto war::Display::width() const -> short
{
    return test_term_width;
}

void war::Display::write_names() 
{
}
auto war::Display::write_score() -> std::array<bool,2>
{
    return { false, false };
}

void war::Display::update() 
{
}
