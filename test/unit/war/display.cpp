#include <catch_main.hpp>

#include <war/display.hpp>

#include <mock/term/output.hpp>
#include <mock/war/named_color.hpp>

using T = war::Display;

namespace {
    struct fix
    {
        std::array<war::Player,2> ps_;
        TermOut term_;
    };
}

TEST_CASE_METHOD(fix, "ctor does not throw" )
{
    CHECK_NOTHROW( T{ps_, {}, term_} );
}

TEST_CASE_METHOD(fix, "write_score jumps to the middle of the top line at some point" )
{
    T t{ps_, {}, term_};
    ps_[0].pile().place_bottom( {fr52::queen, fr52::Suit::Hearts} );
    term_.set_pos_.clear();
    CHECK( t.write_score()[1] );
    CHECK( term_.set_pos_.size()    == 1 );
    CHECK( term_.set_pos_.at(0).x() == 46);
    CHECK( term_.set_pos_.at(0).y() == 1 );
}
