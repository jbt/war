#include <catch_main.hpp>

#include <mock/war/display.hpp>
#include <mock/term/output.hpp>
#include <war/player.hpp>
#include <war/battle_animation.hpp>

using T = war::BattleAnimation;
using P = fr52::Pile;
using S = fr52::Suit;
using D = war::Direction;

namespace {
    struct fix
    {
        std::array<war::Player,2> ps_;
        TermOut term_;
        war::Display disp_ = war::Display{ps_, {}, term_};
    };
}

TEST_CASE_METHOD(fix, "ctor does not throw" )
{
    CHECK_NOTHROW( T{disp_} );
}
TEST_CASE_METHOD(fix, "sweeping nothing does not change piles" )
{
    T t{disp_};
    P p;
    CHECK(p.size() == 0);
    t.sweep( D::Left, p );
    CHECK(p.size() == 0);
}
TEST_CASE_METHOD(fix, "sweeping a pair of cards leaves them in the pile" )
{
    T t{disp_};
    t.place( {2, S::Hearts}, {3, S::Diamonds} );
    P p;
    CHECK(p.size() == 0);
    t.sweep( D::Left, p );
    CHECK(p.size() == 2);
    CHECK(p.draw().rank() == 2);
    CHECK(p.draw().rank() == 3);
}
TEST_CASE_METHOD(fix, "next_level() is next line down." )
{
    T t{disp_};
    t.place( {2, S::Hearts}, {3, S::Diamonds} );
    auto one = term_.set_pos_;
    term_.set_pos_.clear();
    auto u = t.next_level();
    u.place( {2, S::Hearts}, {3, S::Diamonds} );
    auto two = term_.set_pos_;
    for ( auto o : one )
    {
        if ( o.row() == 1 ) {
            //Ignore names and pile sizes.
            continue;
        }
        CHECK(o.row()==2);
    }
    for ( auto e : two )
    {
        if ( e.row() == 1 || e.col() > 90 ) {
            continue;
        }
        if ( e.row() == 2 )
        {
            //The comment in code: //Y position is 2 and not line_? Yes, because no matter how many times we've had and recursed into new battles we don't generate new piles.
            //The card backs are at extreme ends col wise, so expect only those columns
            REQUIRE(e.col() == 1);
        }
        else
        {
            REQUIRE(e.row()==3);
        }
    }
}
