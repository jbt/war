#include <catch_main.hpp>
#include <term/utf8.hpp>

TEST_CASE( "Simple, common, Latin-1" )
{
    CHECK( term::approx_text_width("John") == 4 );
}

TEST_CASE( "Using one non-special multi-byte codepoint" )
{
    //TODO : reintroduce this check after fixing dealing with issue/15
//     CHECK( term::approx_text_width("Jѻhn") == 4 );
}
