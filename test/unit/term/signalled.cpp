#include <term/signalled.hpp>
#include <catch_main.hpp>
#include <csignal>

TEST_CASE( "did_not_signal" )
{
    CHECK( ! term::signalled() );
}
TEST_CASE( "did_signal" )
{
    CHECK( ! term::signalled() );
    std::raise(SIGFPE);
    CHECK( term::signalled() );
    term::disregard_previous_signal();
}

TEST_CASE( "no signal no throw" )
{
    CHECK_NOTHROW( term::throw_if_signalled() );
}
