#include <term/output/iostream.hpp>
#include <catch_main.hpp>

#include <term/color.hpp>

using T = term::output::IOStream;

namespace {
    struct fix
    {
        std::ostringstream s_;
        T t_ = T{s_};
        std::string written() const
        {
            std::string w = s_.str();
            for ( auto i = 0u; (i = w.find('\033')) < w.size(); )
            {
                w[i] = '\\';
                w.insert( i + 1u, 1, 'e' );
            }
            return w;
        }
    };
}

namespace term {
    void throw_if_signalled(){}
}

TEST_CASE_METHOD(fix, "ctor_hides_cursor" )
{
    CHECK(written() == "\\e[?25l");
}
TEST_CASE_METHOD(fix, "writes_latin1_graph" )
{
    s_.str("");
    t_.write("whatever");
    CHECK(written() == "whatever");
}
TEST_CASE_METHOD(fix, "clear_screen" )
{
    s_.str("");
    t_.clear_screen();
    CHECK(written() == "\\e[2J");
}
TEST_CASE_METHOD(fix, "set_color" )
{
    s_.str("");
    t_.set_color(term::Color{0x030405});
    CHECK(written() == "\\e[38;2;3;4;5m");
}
TEST_CASE_METHOD(fix, "bg_color" )
{
    s_.str("");
    t_.set_bg_color(term::Color{0x030405});
    CHECK(written() == "\\e[48;2;3;4;5m");
}
TEST_CASE_METHOD(fix, "set_position" )
{
    s_.str("");
    t_.set_position({1,2});
    CHECK(written() == "\\e[2;1H");
}

namespace {
    struct Flushable : public std::ostream
    {
        struct Buf : public std::streambuf 
        {
            int sync_count_ = 0;
            int sync() override
            {
                ++sync_count_;
                return 0;
            }
        };
        Buf b_;
        Flushable()
        {
            rdbuf( &b_ );
        }
        int flush_count() const
        {
            return b_.sync_count_;
        }
    };
}
TEST_CASE("flush passes through to stream" )
{
    Flushable f;
    T t{ f };
    CHECK(f.flush_count() == 0);
    t.flush();
    CHECK(f.flush_count() == 1);
}
