war
===

War: a Card Non-Game
====================

You've seen card games emulated in software. Normally it handles all the tedium (shuffling, etc.) and only stops when the player needs to make a decision. What if there were no decisions to make? What if the game were just tedium for its own sake?

![What it looks like](https://gitlab.com/jbt/war/-/raw/stable/docs/screen_recording.webm)

## Rules

See a thorough description [here](https://www.pagat.com/war/war.html)

> Most descriptions of War are not clear about what happens if a player runs out of cards during a war. There are at least two possibilities:
> If you don't have enough cards to complete the war, you lose. If neither player has enough cards, the one who runs out first loses.

This is the route I'm going.

> Many players play three face-down cards in a war rather than just one.

This is the variant I learned as a child, so that's what I'm doing.

## Requirements

### To Build

#### conan

The preferred way to install conan is via pip, in particular try:
`pip3 install --user --upgrade conan`

#### cmake

I *strongly* recommend you use your distro's preferred package manager. For my laptop that would be:
`nix-env -i cmake`

For my family's desktop it's:
`sudo apt install cmake`

If you're really unsure, you can try instructions found [here](https://cmake.org/install/).

If all else fails, you already have conan installed and you technically can fetch a functioning cmake through conan. This isn't the use case that was intended for - if you know how to do this well you probably have access to a better means of installing cmake.

#### build tool (ninja/make)

Some build tool (possibly bundled with an IDE) that cmake can use. I prefer ninja, but make seems to be the default.

Whichever you choose, it needs to support [CMAKE_EXPORT_COMPILE_COMMANDS](https://cmake.org/cmake/help/latest/variable/CMAKE_EXPORT_COMPILE_COMMANDS.html?highlight=cmake_export_compile_commands)

#### A C++20-compliant compiler

That is, the ISO version of C++ and its standard library that was (will be) released in 2020.

If you can't do that, you could switch to C++17 in the CMakeLists.txt with only a couple minor code edits. For example, I use CTAD in a couple of places but the type being deduced is blatantaly obvious, so instead you could just write out the template arguments.

If your compiler can't handle 2017-era C++, c'mon, do yourself a favor and get a better compiler. I won't accept pull requests meant to make it compile with ancient compilers.

### To Run

Yeah, naturally a C++ runtime that's compatible with your toolchain.

But, also... a terminal or (more likely) terminal emulator that:

* Supports and correctly displays UTF-8 text
* A monospaced(ish) font that has a bunch of weird glyphs, in particular anything in your name and also things like `U+1F0A0 PLAYING CARD BACK` and `U+1F0A1 PLAYING CARD ACE OF SPADES`
* Supports a wide range of ANSI escape codes

I recommend gnome-terminal or konsole, but there a fair number of others for Linux in particular. This requirement will be rather obnoxious for people on Windows. There is a fork of PuTTY uses a local cygwin shell. Cygwin is janky as heck but it always seems to be the 'answer' for people stuck in Windows, doesn't it?

