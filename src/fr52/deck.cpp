#include "deck.hpp"

static_assert( fr52::full_deck.size() == 52 );
static_assert( fr52::full_deck.capacity() == 52 );
static_assert( sizeof(fr52::full_deck) >= 52 );

constexpr auto force_a_defrag()
{
    fr52::Pile p;
    for ( auto i = 0; i < 65539; ++i ) //considerably more than capacity
    {
        p.place_bottom( {} );
        p.draw();
    }
    return p.empty();
}

#ifndef __clang__ // clang is convinced force_a_defrag is not constexpr, despite compiling it with the keyword

    constexpr bool ending_empty = force_a_defrag();
    static_assert( ending_empty );

#endif

