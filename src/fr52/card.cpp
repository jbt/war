#include "card.hpp"

using C = fr52::Card;
using S = fr52::Suit;

static_assert( C{9, S::Hearts}.rank() == 9 );

static_assert( fr52::king < fr52::ace  );
static_assert( fr52::king > fr52::queen);
static_assert( fr52::jack < fr52::queen);

//constexpr auto default_card = C{};

static_assert( C{9, S::Hearts}.red() );

static_assert( C{9, S::Hearts}.unicode() == "🂹" );
