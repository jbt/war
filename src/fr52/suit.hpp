#pragma once

#include <cassert>
#include <cstdint>

namespace fr52 {

    /**
     * @brief Only the suits which are possible in the standard 52-card deck
     * @details The numeric values are used elsewhere. Do not change them.
     */
    enum class Suit : std::int8_t
    {
        Hearts  ,
        Diamonds,
        Clubs   , 
        Spades
    };
    
    constexpr bool valid( Suit s )
    {
        switch ( s )
        {
        case Suit::Hearts  : return true;
        case Suit::Diamonds: return true;
        case Suit::Clubs   : return true;
        case Suit::Spades  : return true;
        default:
            return false;
        }
    }
    
    constexpr bool red( Suit s )
    {
        switch ( s )
        {
        case Suit::Hearts  : return true;
        case Suit::Diamonds: return true;
        case Suit::Clubs   : return false;
        case Suit::Spades  : return false;
        default:
            assert(false);
        }
    }
    
    constexpr bool black( Suit s ) {
        return !red( s );
    }
    
}
