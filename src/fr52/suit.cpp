#include "suit.hpp"

using S = fr52::Suit;

static_assert(valid(S::Hearts));
static_assert(black(S::Spades));

