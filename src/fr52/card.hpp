#pragma once

#include <fr52/suit.hpp>
#include <boost/beast/core/static_string.hpp>
#include <cassert>
#include <string_view>

/**
 * @brief The 52-card standard/French playing card deck
 * @details The code in this namespace assumes a *very* standard deck. Not Pinochle. Not double-decked Rummy. No Jokers.
 */
namespace fr52 {

    /**
     * @brief   Data type appropriate for storing the rank of a card
     * @details Must support operator<()
     *          numeric ranks are their own values, e.g. a two of hearts has the rank valued 2
     *          10 < J < Q < K < A
     */
    using Rank = std::int8_t;
    constexpr Rank jack = 11;///< A jack  is one higher than 10
    constexpr Rank queen= 12;///< A queen is one higher than jack
    constexpr Rank king = 13;///< A king  is one higher than queen
    constexpr Rank ace  = 14;///< An ace  is one higher than king

    /**
     * @brief Simple representation of a card. Has a rank (e.g. 2 or Ace) and  a Suit
     * @note A card is invariant and treated as a single value. There's no such thing as set_suit().
     */
    class Card
    {
    public:
        Card() = default;

        /**
         * @brief Construct a card provided all the state up-front
         * @param r The rank (e.g. number) of the card
         * @param s The suit of the card
         */
        constexpr Card( Rank r, Suit s )
        : rank_{r}
        , suit_{s}
        {
            assert(valid());
        }
        
        /**
         * @brief get the rank of the card
         * @return the numeric value or a face card's rank converted to numeric value in the proper ordering
         */
        [[nodiscard]] constexpr auto rank() const {
            return rank_;
        }
        
        /**
         * @brief get the suit of the card
         * @return An enum for the pip
         */
        [[nodiscard]] constexpr auto suit() const {
            return suit_;
        }
        
        /**
         * @brief Some basic class invariant checks
         * @return true iff *this is known to be in a valid state
         * @note This is generally only used if ! NDEBUG
         */
        constexpr bool valid()
        {
            if ( rank() < 2 ) {
                return false;
            }
            if ( rank() > 14 ) {
                return false;
            }
            return fr52::valid(suit_);
        }
        
        /**
         * @brief Determine the card's color (i.e. font foreground color)
         * @return true iff it's a heart or a diamond, i.e. not black
         */
        constexpr auto red() const
        {
            return fr52::red( suit() );
        }
        
        /**
         * @brief  a unicode representation of the card
         * @return a string view into a single codepoint
         */
        constexpr auto unicode()
        {
            auto index = static_cast<unsigned>( (rank_ - 2) * 4 + static_cast<int>(suit_) );
            return reps
                #ifdef NDEBUG
                    [index]
                #else
                    .at(index)
                #endif
                ;
        }
                
    private:
        Rank rank_ = 2; //TODO(John) - cutting sizeof(Card) in half would make things like Pile more performant. Maybe worth measuring?
        Suit suit_ = Suit::Hearts;
        
        static constexpr auto reps = [](){
            using namespace std::literals;
                return std::array{
                    "🂲"sv,
                    "🃂"sv,
                    "🃒"sv,
                    "🂢"sv,
                    "🂳"sv,
                    "🃃"sv,
                    "🃓"sv,
                    "🂣"sv,
                    "🂴"sv,
                    "🃄"sv,
                    "🃔"sv,
                    "🂤"sv,
                    "🂵"sv,
                    "🃅"sv,
                    "🃕"sv,
                    "🂥"sv,
                    "🂶"sv,
                    "🃆"sv,
                    "🃖"sv,
                    "🂦"sv,
                    "🂷"sv,
                    "🃇"sv,
                    "🃗"sv,
                    "🂧"sv,
                    "🂸"sv,
                    "🃈"sv,
                    "🃘"sv,
                    "🂨"sv,
                    "🂹"sv,
                    "🃉"sv,
                    "🃙"sv,
                    "🂩"sv,
                    "🂺"sv,
                    "🃊"sv,
                    "🃚"sv,
                    "🂪"sv,
                    "🂻"sv,
                    "🃋"sv,
                    "🃛"sv,
                    "🂫"sv,
                    "🂽"sv,
                    "🃍"sv,
                    "🃝"sv,
                    "🂭"sv,
                    "🂾"sv,
                    "🃎"sv,
                    "🃞"sv,
                    "🂮"sv,
                    "🂱"sv,
                    "🃁"sv,
                    "🃑"sv,
                    "🂡"sv
                };
            }();
    };

}  // namespace fr52
