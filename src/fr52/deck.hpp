#pragma once

#include <fr52/card.hpp>
#include <algorithm>
#include <array>

namespace fr52 {

    /**
     * @brief   A pile of cards, not necessarily the whole deck.
     * @details Since cards get placed on the bottom, and pulled off the top, it does need to maintain a specific order internally.
     */
    class Pile
    {
    public:
        
        /**
         * @brief A datatype reasonable for storing any possible index into a 52-card deck.
         */
        using Index = std::int_fast16_t;
        
    private:
        using Storage = std::array<Card,52>;
        Storage cards_ = Storage{
            Card{ 2, Suit::Hearts}, Card{ 2, Suit::Diamonds}, Card{ 2, Suit::Clubs}, Card{ 2, Suit::Spades}
          , Card{ 3, Suit::Hearts}, Card{ 3, Suit::Diamonds}, Card{ 3, Suit::Clubs}, Card{ 3, Suit::Spades}
          , Card{ 4, Suit::Hearts}, Card{ 4, Suit::Diamonds}, Card{ 4, Suit::Clubs}, Card{ 4, Suit::Spades}
          , Card{ 5, Suit::Hearts}, Card{ 5, Suit::Diamonds}, Card{ 5, Suit::Clubs}, Card{ 5, Suit::Spades}
          , Card{ 6, Suit::Hearts}, Card{ 6, Suit::Diamonds}, Card{ 6, Suit::Clubs}, Card{ 6, Suit::Spades}
          , Card{ 7, Suit::Hearts}, Card{ 7, Suit::Diamonds}, Card{ 7, Suit::Clubs}, Card{ 7, Suit::Spades}
          , Card{ 8, Suit::Hearts}, Card{ 8, Suit::Diamonds}, Card{ 8, Suit::Clubs}, Card{ 8, Suit::Spades}
          , Card{ 9, Suit::Hearts}, Card{ 9, Suit::Diamonds}, Card{ 9, Suit::Clubs}, Card{ 9, Suit::Spades}
          , Card{10, Suit::Hearts}, Card{10, Suit::Diamonds}, Card{10, Suit::Clubs}, Card{10, Suit::Spades}
          , Card{ jack, Suit::Hearts}, Card{ jack, Suit::Diamonds}, Card{ jack, Suit::Clubs}, Card{ jack, Suit::Spades}
          , Card{queen, Suit::Hearts}, Card{queen, Suit::Diamonds}, Card{queen, Suit::Clubs}, Card{queen, Suit::Spades}
          , Card{ king, Suit::Hearts}, Card{ king, Suit::Diamonds}, Card{ king, Suit::Clubs}, Card{ king, Suit::Spades}
          , Card{  ace, Suit::Hearts}, Card{  ace, Suit::Diamonds}, Card{  ace, Suit::Clubs}, Card{  ace, Suit::Spades}
        };
        Index begin_= 0;
        Index end_  = 0;
        
        /**
         * @brief maintain some invariants
         * @note This isn't constexpr. If you call it at compile time your compilation will fail, because you shouldn't get into that circumstance.
         */
        void defrag() 
        {
            auto sz = size();
            begin_ %= capacity();
            end_ = begin_ + sz;
            if ( end_ > capacity() )
            {
                end_ -= capacity();
                std::rotate(
                    std::next( cards_.begin(), end_  )
                  , std::next( cards_.begin(), begin_)  
                  , cards_.end()
                  );
                end_ += capacity() - begin_;
                begin_ = 0;
            }
            assert( sz == size() );
        }
        constexpr auto at( Index i ) -> Card& 
        {
            assert( i >= 0 );
            return cards_[ static_cast<unsigned>(i % capacity()) ];
        }

    public:

        /**
         * @brief construct a pile containing a given number of arbitrarily selected unique cards
         * @note  It's hard to imagine a circumstance where how_many is anything other than 0 or 52
         * @param how_many How many cards would you like?
         * @pre 0 <= how_many <= 52
         */
        constexpr explicit Pile( Index how_many = 0 )
        : end_{ how_many }
        {
            assert( end_ >= 0 );
            assert( end_ <= capacity() );
        }

        /**
         * @brief  How many cards are currently contained
         * @return A non-zero integer, which may nonetheless be signed
         */
        constexpr auto size() const -> short { return static_cast<short>(end_ - begin_); }
        
        /**
         * @brief  Is the pile empty and therefore no longer a pile by most definitions
         * @return size() == 0
         */
        constexpr auto empty() const { return size() == 0; }
        
        constexpr auto capacity() const -> long
        {
            return static_cast<long>( cards_.size() );
        }

        
        constexpr auto draw()
        {
            assert(size()>0);
            return at(begin_++);
        }
        constexpr void place_bottom( Card c )
        {
            assert( size() < 52 );
            at( end_++ ) = c;
            if ( end_ < 0 ) { 
                defrag();
            }
        }
        
        void shuffle( auto rng )
        {
            defrag();
            std::shuffle(
                std::next(cards_.begin(), begin_)
              , std::next(cards_.begin(), end_  )
              , rng
              );
        }

    };

    /**
     * @brief convenience in case you need a whole deck of cards some time
     */
    constexpr auto full_deck = Pile{ 52 };
}
