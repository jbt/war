#include <war/game.hpp>
#include <war/named_color.hpp>
#include <boost/program_options.hpp>
#include <iostream>

namespace po = boost::program_options;

/**
 * @brief   Program entry point. Parameters are command line arguments. Use --help for more info.
 * @details This function does little more than parse the command line and if appropriate kicks off a game by calling war::game()
 * @sa      war::game(Opts const&)
 * @param   argc
 *      The count of arguments, including the first one which refers to the program itself
 * @param   argv
 *      The command line arguments. For this program argv[0] is ignored.
 * @return  The program's exit code: 0 (success) upon normal termination. Other exit codes indicate a bug was detected (and most likely abort() was called()).
 * @author  John
 * @date    2020-07-01
 */
int main( int argc, char const* argv[] )
{
    try {
        po::options_description desc;
        desc.add_options()
            ("help,h"   , "Output this help message."         )
            ("version,v", "Output the current version of war.")
            ("player1,1", po::value<std::string>()->default_value("Player 1"), "The name of the  left-hand player. N.B.: Non-Latin-1 characters can mess things up sometimes certain control & flow characters *really* can.")
            ("player2,2", po::value<std::string>()->default_value("Player 2"), "The name of the right-hand player. Also, especially long names will mess things up badly.")
            ("speed,s"  , po::value<long>()->default_value(100)              , "Speed of the game's animation, lower numbers are faster." )
            ("seed"     , po::value<long>()->default_value(-1L)              , "The seed to use for the pseudorandom number generator that determines the deck's shuffle. A negative value indicates one should be chosen based on the system clock instead. If you want a truer randomness, consider --seed=${RANDOM} .")
            ;
        std::string const color_opt_prefix = "color_";
        for ( auto c : magic_enum::enum_values<war::NamedColor>() )
        {
            std::ostringstream def_val;
            def_val << '#' << std::hex << get(c).rgb_code();
            auto n = std::string{name(c)};
            desc.add_options()
                (   (color_opt_prefix + n).c_str()
                , po::value<std::string>()->default_value(def_val.str())
                , ("Color for displaying a '" + n + "'").c_str()
                );
        }
        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        if ( vm.count("help") )
        {
            std::cout << desc << "\n";
            return 0;
        }
        if ( vm.count("version") )
        {
            std::cout << WAR_VERSION << '\n';
            return 0;
        }
        po::notify(vm);    
        for ( auto c : magic_enum::enum_values<war::NamedColor>() )
        {
            auto n = std::string{name(c)};
            std::istringstream iss{ vm[color_opt_prefix+n].as<std::string>() };
            iss.get();//Discard #
            war::rgb_int rgb;
            iss >> std::hex >> rgb;
            assign_rgb( c, rgb ); 
        }
        war::game( vm );
    } catch ( std::exception const& ex ) {
        std::cout << "\nError encountered: " << ex.what() << std::endl;
        std::clog << "\nError encountered: " << ex.what() << std::endl;
        return 1;
    }
}
