#pragma once

#include "battle_animation.hpp"

namespace war {
        
    class Player;

    /**
     * Perform a round aka a battle
     * @param p1   The player on the left
     * @param p2   The player on the right
     * @param anim Object that handles the display of the action relate to this particular round
     * @return whether p1 won the battle. 
     * @note This return value may not actually be useful.
     */
    bool round( Player& p1, Player& p2, BattleAnimation& anim );

}
