#pragma once

#include <fr52/deck.hpp>

namespace war {

    /**
     * @brief Connects together some info about one of the players.
     */
    class Player
    {
        fr52::Pile cards_ = fr52::Pile{};
        std::string_view name_ = "Unknown player";
    public:
        
        /**
         * @brief Accessor for a this player's stack of cards.
         * @return a pile of cards
         */
        auto cards() const -> decltype(cards_) const& { return cards_; }
        
        /**
         * @brief A mutable version of cards()
         * @details The name is a reference to the underlying structure since this is the whitebox version where you get to mutate my guts
         * @return a Pile of cards
         */
        auto pile() -> decltype(cards_)& { return cards_; }
        
        /**
         * @brief How many cards does the player have remaining in their stack?
         * @return A count in the range [0,52]
         */
        auto remaining() const { return cards_.size(); }
        
        /**
         * @brief Whether the player is out of cards signalling the end of the game.
         * @return bool true iff remaining() == 0
         */
        auto out() const { return remaining() == 0; }
        
        /**
         * @brief accessor for the player's name
         * @return A read-only string-like
         */
        auto name() const { return name_; }
        
        /**
         * @brief Setter for the player's name
         * @param n How the player should be called from now on.
         * @todo Sanitize or forbid problematic names.
         */
        void name( std::string_view n ) { name_ = n; }
    };

}
