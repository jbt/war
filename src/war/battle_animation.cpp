#include "battle_animation.hpp"
#include <war/display.hpp>
#include <war/named_color.hpp>
#include <term/color.hpp>
#include <term/utf8.hpp>

using BA = war::BattleAnimation;

BA::BattleAnimation( Display& parent )
: line_{2}
, parent_{parent}
, max_col_{ static_cast<short>(parent.width() - 7) }
{
    write_piles();
}

BA::BattleAnimation( BattleAnimation const& prev, short line )
: line_{ line }
, parent_{  prev.parent_  }
, max_col_{ prev.max_col_ }
{
    write_piles();
}

BA::~BattleAnimation()
{
    write_piles();
}

BA BA::next_level()
{
    return BA{ *this, static_cast<short>(line_ + 1) };
}

namespace {
    constexpr std::string_view card_back{"🂠"};
    term::Color front_color( fr52::Card const& c )
    {
        using war::NamedColor;
        return c.red()
            ? get(NamedColor::red_suit_font)
            : get(NamedColor::black_suit_font)
            ;
    }
}

void BA::write_piles()
{
    std::array<short,2> cols = {
        1,
        static_cast<short>(parent_.width() - 4)
    };
    for ( auto idx : {0u, 1u} )
    {
        if ( cols[idx] <= 0 ) {
            continue;
        }
        //Y position is 2 and not line_? Yes, because no matter how many times we've had and recursed into new battles we don't generate new piles.
        out().set_position( {cols[idx], 2} )
            .set_color( get(NamedColor::card_back) )
            .write( " " )
            .write( card_back )
            .write( " " )
            ;
    }
    auto empty = parent_.write_score();
    for ( auto idx : {0u, 1u} )
    {
        if ( empty[idx] ) {
            out().set_position( {cols[idx], 2} ).write( "   " );
        }
    }
    if ( empty[0] || empty[1] ) {
        out().flush();
    }
}

void BA::place( Card left, Card right, Orientation how )
{
    write_piles();
    played_.emplace( played_.begin(), left, how, min_col_ );
    auto last_index = static_cast<unsigned>( played_.size() );
    played_.emplace_back( right, how, max_col_ );
    while ( move(0u, Direction::Right) && move(last_index, Direction::Left) ) {
        parent_.update();
    }
}

auto BA::sweep( Direction whereto, Pile& into ) -> void
{
    while ( played_.size() > 0u )
    {
        while ( shift(whereto) ) {
            parent_.update();
        }
        auto it = ( Direction::Left == whereto )
            ? played_.begin()
            : std::prev( played_.end() )
            ;
        into.place_bottom( it->card_ );
        out().set_position( {it->pos_, line_} ).write( "   " );
        played_.erase( it );
        write_piles();
    }
}

auto BA::shift( Direction dir ) -> bool
{
    auto any_shifted = false;
    for ( auto i = 0u; i < played_.size(); ++i )
    {
        if ( move(i, dir) ) {
            any_shifted = true;
        }
    }
    return any_shifted;
}

auto BA::move( unsigned index, Direction dir ) -> bool
{
    assert( index < played_.size() );
    auto& c = played_[index];
    constexpr auto space_between_cards = 3;
    if ( dir == Direction::Left )
    {
        if ( c.pos_ <= min_col_ ) {
            return false;
        }
        if ( index && c.pos_ - played_[index-1].pos_ <= space_between_cards ) {
            return false;
        }
        c.pos_--;
        //TODO(bug 19) extract the writing of a single card into a function - watch out for the subtle difference
        out().set_position( {c.pos_, line_} ).write( " " );
        if ( c.how_ == Orientation::FaceDown )
        {
            out().set_color( get(NamedColor::card_back) ).write( card_back );
        }
        else
        {
            out().set_color( front_color(c.card_) ).write( c.card_.unicode() );
        }
        out().write( "  " );
    }
    else
    {
        if ( c.pos_ >= max_col_ ) {
            return false;
        }
        if ( played_.size() - index > 1 && played_[index+1].pos_ - c.pos_ <= space_between_cards ) {
            return false;
        }
        out().set_position( {c.pos_++, line_} ).write( "  " );
        if ( c.how_ == Orientation::FaceDown )
        {
            out().set_color( get(NamedColor::card_back) ).write( card_back );
        }
        else
        {
            out().set_color( front_color(c.card_) ).write( c.card_.unicode() );
        }
        out().write( " " );
    }
    return true;
}

auto BA::out() -> term::Output&
{
    return parent_.output();
}
