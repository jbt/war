#pragma once

#include "player.hpp"
#include "battle_animation.hpp"
#include <term/output.hpp>
#include <array>
#include <chrono>

namespace war {
    
    /**
     * @brief   A class that manages the 'display' from a high level.
     * @details contains logic for e.g. where the player names go. Delegates actual display issues to a term::Output
     */
    class Display
    {
    public:
        using Slowness = std::chrono::system_clock::duration;///< A datatype appropriate for storing a moderately short period of time (on the order of ms)

    private:
        std::array<Player,2> const& players_;
        Slowness const slowness_;
        std::array<short,2> pile_sizes_;
        short prev_width_ = 0;
        term::Output& term_;
        
        auto midpoint() const { return width() / 2; }
        void write_names();

    public:
        
        /**
         * @brief Construct a display with all the data it needs to show what it needs to show
         * @param players  reference to the data about the 'people' playing the 'game'
         * @param slowness How much time passes between the miniscule steps during e.g. an animation
         * @param term_out The terminal-like object to write output to. As the type implies, *this takes ownership of it.
         */
        Display( std::array<Player,2> const& players, Slowness slowness, term::Output& term_out );

        ~Display();

        /**
         * @brief write the card pile sizes to the terminal
         * @return two bool indicating whether player one and player two, respectively, have an empty pile
         */
        std::array<bool,2> write_score();
        
        /**
         * @brief update the full display, in particular the sizes of the players' stacks
         */
        void update();
        
        /**
         * @brief How wide is the terminal, in terms of columns.
         * @return a non-negative column count
         */
        auto width() const -> short;
        
        /**
         * @brief  Get access to the output being written to
         * @return mutable reference to a term::Output
         */ 
        term::Output& output() { return term_; }
        
        /**
         * @brief  Get access to the output being written to
         * @return mutable reference to a term::Output
         */ 
        term::Output const& output() const { return term_; }
    };
    
}
