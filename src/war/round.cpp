#include "round.hpp"

#include "player.hpp"
#include <iostream>

bool war::round( Player& p1, Player& p2, BattleAnimation& animation )
{
    if ( p1.out() ) {
        return false;
    }
    if ( p2.out() ) {
        return true;
    }
    auto c1 = p1.pile().draw();
    auto c2 = p2.pile().draw();
    animation.place( c1, c2 );
    if ( c1.rank() > c2.rank() )
    {
        animation.sweep( Direction::Left , p1.pile() );
        return true;
    }
    else if ( c1.rank() < c2.rank() )
    {
        animation.sweep( Direction::Right, p2.pile() );
        return false;
    }
    else
    {
        for ( auto i = 0; i < 3; ++i )
        {
            if ( p1.out() || p2.out() ) {
                return p2.out();
            }
            animation.place( p1.pile().draw(), p2.pile().draw(), Orientation::FaceDown );
        }
        auto nl{ animation.next_level() };
        if ( round(p1, p2, nl) )
        {
            animation.sweep( Direction::Left , p1.pile() );
            return true;
        }
        else
        {
            animation.sweep( Direction::Right, p2.pile() );
            return false;
        }
    }
}
