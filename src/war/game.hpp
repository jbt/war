#pragma once

#include <boost/program_options/variables_map.hpp>
#include <array>
#include <string_view>

/**
 * @brief A namespace to contain code that feels specific to this game.
 */
namespace war { 
 
    /**
     * @brief program options, which are currently only coming from command line arguments
     */	
    using Opts = boost::program_options::variables_map;
    
    /**
     * @brief Play a game. A single 'war'
     * @param parameters Choices for how the game is to be played
     */
    void game( Opts const& parameters );
    
}
