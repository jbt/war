#include "game.hpp"
#include "display.hpp"
#include "player.hpp"
#include "round.hpp"
#include <fr52/deck.hpp>
#include <term/output/iostream.hpp>
#include <term/color.hpp>
#include <chrono>
#include <iostream>
#include <random>

namespace {
    using Players = std::array<war::Player,2>;
    
    void deal( Players& ps, unsigned long seed )
    {
        //Get the deck, shuffle
        auto deck = fr52::full_deck; //by-value copy
        std::default_random_engine randomness{ seed };
        deck.shuffle( randomness );

        //Deal all the cards
        while ( ! deck.empty() )
        {
            for ( auto& p : ps ) {
                p.pile().place_bottom( deck.draw() );
            }
        }
    }
}

auto war::game( Opts const& parameters ) -> void
{
    auto const& o = parameters;
    auto n1  = o["player1"].as<std::string>();
    auto n2  = o["player2"].as<std::string>();
    auto seed= o["seed"   ].as<long>();
    auto slow= o["speed"  ].as<long>();
    
    Players ps;
    ps[0].name( n1 );
    ps[1].name( n2 );

    if ( seed < 0 ) {
        seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    }
        
    deal( ps, static_cast<unsigned long>(seed) );
    
    auto player_has_lost = [](auto&p){return p.out();};
    
    term::output::IOStream output{ std::cout };
    Display display{ 
          ps
        , std::chrono::milliseconds(slow) 
        , output
    };
    
    while ( std::none_of(ps.begin(), ps.end(), player_has_lost) ) {
        assert( ps[0].remaining() + ps[1].remaining() == 52 );
        BattleAnimation ba{ display };
        round( ps[0], ps[1], ba );
        display.write_score();
    }
    output.set_color( term::white );
    if ( std::all_of(ps.begin(), ps.end(), player_has_lost) ) {
        output.write( "\n\n The game is a draw.\n" );
    }
    for ( auto & p : ps )
    {
        if ( p.remaining() > 0 )
        {
            output
                .write( "\n\n " )
                .write( p.name() )
                .write( " wins the game!\n" )
                ;
        }
    }
    std::cout << "\n\t(seed was " << seed << ")\n";
}
