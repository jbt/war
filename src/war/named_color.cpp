#include "named_color.hpp"

using namespace std::literals;
using NC = war::NamedColor;

namespace {
    
    constexpr auto defined_colors_count = magic_enum::enum_count<NC>();
    std::array<war::rgb_int,defined_colors_count> defined_colors = {
        0x248fFf // card_back
      , 0xFf0000 // red_suit
      , 0x000000 // black_suit
      , 0xFfFfFf // table_top
      , 0x204030 // player_name
    };
    
    constexpr auto index( NC nc )
    {
        return static_cast<std::size_t>( magic_enum::enum_integer(nc) );
    }
}

auto war::get(NamedColor nc) -> term::Color
{ 
    return term::Color{ defined_colors[index(nc)] };
}

void war::assign_rgb( NamedColor nc, rgb_int rgb_code )
{ 
    defined_colors[index(nc)] = rgb_code;
}

static_assert( name(NC::table_top) == "table_top"sv );
