#pragma once

#include <term/color.hpp>
#include <magic_enum.hpp>
#include <string_view>
#include <cstdint>

namespace war {

    /**
     * @brief an integral data type capable of holding 3 unsigned-8-bit channels in r-g-b signficance order
     */
    using rgb_int = std::int_least32_t;
    
    /**
     * @brief Some of the colors from the game named by their usage.
     */
    enum class NamedColor : char
    {
        card_back
      , red_suit_font
      , black_suit_font
      , table_top  
      , player_name
    };
    
    /**
     * @brief Get the name as a runtime-useful string
     * @param nc Which color's name are we seeking
     * @details The name exactly corresponds to the name of the enumeration value seen above
     */
    constexpr std::string_view name(NamedColor nc) { 
        return magic_enum::enum_name(nc); 
    }
    
    /**
     * @brief  convert the name into the color
     * @return a copy of the constant
     */
    term::Color get( NamedColor );
    
    /**
     * @brief Change the RGB value of the color used for a particular case
     * @param nc  The name of the color being changed
     * @param rgb Least significant octet: blue channel (unsigned); rgb / 256 % 256 is green channel, etc. High octet should be zero.
     */
    void assign_rgb( NamedColor nc, rgb_int rgb );
}
