#pragma once

#include <fr52/card.hpp>
#include <boost/container/small_vector.hpp>

namespace fr52 {
    class Pile;
}
namespace term {
    class Output;
}

namespace war {
    
    class Display;
    
    enum class Orientation
    {
        Absent,
        FaceUp,
        FaceDown
    };
    enum class Direction
    {
        Right,
        Left
    };

    /**
     * @brief Receives info about what is going on in the game and decides what glyphs should go where on the terminal.
     */
    class BattleAnimation
    {
    public:
        using Card = fr52::Card;///< A value type containing info about a particular card without context
        using Pile = fr52::Pile;///< A pile of cards that could be visually represented
        
    private:
        short const line_;
        Display& parent_;
        short const min_col_ = 4;
        short const max_col_;
        
        struct PlayedCard
        {
            Card        card_= {};
            Orientation how_ = Orientation::Absent;
            short       pos_ = 0;
            
            PlayedCard( Card c, Orientation o, short p )
            : card_{c}
            , how_{o}
            , pos_{p}
            {}
        };
        boost::container::small_vector<PlayedCard,8> played_ = {};

    public:
        
        /**
         * @brief Construct an object that handles the animation of a typical battle 
         * @param parent Where to draw the animation
         */
        BattleAnimation( Display& parent );
        
        BattleAnimation( BattleAnimation const& parent ) = default;

        /**
         * @brief Construct an sub-battle animation, for handling a tiebreak
         * @param prev The previous battle, which ended in a tie / draw
         * @param line On which row of the display does this battle occur
         */
        explicit BattleAnimation( BattleAnimation const& prev, short line );

        ~BattleAnimation();

        /**
         * @brief place a pair of cards on the table
         * @param left  The card which is coming from the player on the left
         * @param right The card which is coming from the player on the right
         * @param how   Whether the face of the card is visible
         */
        void place( Card left, Card right, Orientation how = Orientation::FaceUp );
        
        /**
         * @brief move all the cards in the row off to the side and to the bottom of one player's pile
         * @param whereto Are they visibly moving to the left or right?
         * @param into Whose pile are the cards going to the bottom of? Hopefully matches whereto logically
         */
        void sweep( Direction whereto, Pile& into );

        /**
         * @brief  Get a context to perform a battle animation one row deeper than this one
         * @return BattleAnimation referencing the same output, but row+1
         */
        BattleAnimation next_level();
        
    private:

        auto write_piles() -> void;
        auto move( unsigned index, Direction dir ) -> bool;
        auto shift( Direction ) -> bool;
        term::Output& out();
    };
    
}
