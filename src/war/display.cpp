#include "display.hpp"
#include <war/named_color.hpp>
#include <term/color.hpp>
#include <term/utf8.hpp>
#include <iostream>
#include <thread>

using D = war::Display;

D::Display( std::array<Player,2> const& players, Slowness slowness, term::Output& term_out )
: players_{ players }
, slowness_{ slowness }
, pile_sizes_{ {0, 0} }
, term_{ term_out }
{
    write_score();
}

D::~Display()
{
    output().reset_mode().flush();
}

auto D::width() const -> short
{
    return prev_width_;
}

void D::write_names() 
{
    term_.set_color( get(NamedColor::player_name) ).set_position( {3, 1} ).write( players_[0].name() );
    auto start = static_cast<short>( width() - term::approx_text_width( players_[1].name() ) - 3 );
    term_.set_position( {start, 1} ).write( players_[1].name() );
}
auto D::write_score() -> std::array<bool,2>
{
    auto w = static_cast<short>(term_.get_width() - 1);
    if ( w != prev_width_ )
    {
        term_.set_bg_color( get(NamedColor::table_top) );
        term_.clear_screen();
        prev_width_ = w;
        write_names();
    }
    for ( auto idx : {0u, 1u} )
    {
        if ( pile_sizes_[idx] == players_[idx].remaining() ) {
            continue;
        }
        pile_sizes_[idx] = players_[idx].remaining();
        auto offset = idx ? 3 : -3;
        auto col = static_cast<short>( midpoint() + offset );
        term_.set_position( {col, 1} );
        auto num = pile_sizes_[idx];
        auto color_comp = [num]( short num_at_max ) -> term::Color::Channel {
                auto dist = std::abs( num - num_at_max );
                if ( dist > 26 ) {
                    return 0;
                }
                auto bright = 26 - dist;
                return static_cast<term::Color::Channel>( 255 * bright / 26 );
            };
        auto color = term::Color{
              color_comp( 0)
            , color_comp(26)
            , color_comp(52)
            };
        term_.set_color( color ).write( " " ).write( std::to_string(num) ).write( " " );
    }
    update();
    return {
        pile_sizes_[0] == 0
      , pile_sizes_[1] == 0
    };
}

void D::update() 
{
    output().flush();
    std::this_thread::sleep_for( slowness_ );
}
