#include "position.hpp"

using P = term::Position;

static_assert( P{1,2}.col() == 1 );
static_assert( P{1,2}.row() == 2 );
static_assert( P{1,2}.x() == 1 );
static_assert( P{1,2}.y() == 2 );

static_assert( P{1,2} < P{1,3} );

constexpr bool less_is_less = P{1,9} < P{2,1};
static_assert(less_is_less);
