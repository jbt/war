#include "color.hpp"

using C = term::Color ;

static_assert( C{} == term::black );

constexpr auto green_channel( C c ) { return c.g; }

static_assert( green_channel(term::red) == 0 );

static_assert( term::black < term::white );

constexpr auto ugly_blue = 0x123456;
static_assert( C{ugly_blue}.r == 0x12 );
static_assert( C{ugly_blue}.g == 0x34 );
static_assert( C{ugly_blue}.b == 0x56 );

static_assert( C{ugly_blue}.rgb_code() == ugly_blue );
