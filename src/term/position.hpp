#pragma once

//#include <compare>
#include <cassert>
#include <cstdint>

namespace term {

    /**
     * @brief represents a position on the terminal that e.g. a cursor could be at
     */
    class Position
    {
        std::int_least16_t const col_;
        std::int_least8_t  const row_;

    public:

        /**
         * @brief construct a position with the given coordinates
         * @param xx The column represented
         * @param yy The row represented
         */
        constexpr Position( auto xx, auto yy )
        : col_{ static_cast<decltype(col_)>(xx) }
        , row_{ static_cast<decltype(row_)>(yy) }
        {
            assert(xx>0);
            assert(yy>0);
        }
        
        constexpr short   x() const { return col_; } ///< @return the x coordinate aka column
        constexpr short   y() const { return row_; } ///< @return the y coordinate aka row
        constexpr short col() const { return col_; } ///< @return the x coordinate aka column
        constexpr short row() const { return row_; } ///< @return the y coordinate aka row
        
        //auto operator<=>(Position const&) const = default;
        constexpr bool operator<( Position const& rhs ) const
        {
            if ( col() != rhs.col() )
            {
                return col() < rhs.col();
            }
            else
            {
                return row() < rhs.row();
            }
        }
    };
    
}
