#include "signalled.hpp"

#include <csignal>
#include <algorithm>
#include <array>

namespace {
    constexpr auto handled_sigs = std::array{
          SIGABRT
        , SIGFPE
        , SIGINT
        , SIGTERM
        /*
        Allowable in the standard, not appropriate for us
        , SIGILL
        , SIGSEGV 
        */
        };
    constexpr auto uninit = 1L + *std::max_element( handled_sigs.begin(), handled_sigs.end() );
    constexpr auto none = uninit + 1L;
    volatile std::sig_atomic_t sig_stat = uninit;
    
    void signal_handler(int signal)
    {
        sig_stat = signal;
    }

}

bool term::signalled()
{
  if ( sig_stat == uninit )
  {
      for ( auto sig : handled_sigs ) {
        std::signal( sig, signal_handler );
      }
      return false;
  }
  else
  {
      return sig_stat != none;
  }
}

void term::disregard_previous_signal()
{
    sig_stat = none;
}

void term::throw_if_signalled()
{
    if ( signalled() )
    {
        auto msg = "Received signal " + std::to_string(sig_stat);
        disregard_previous_signal();
        throw std::runtime_error{ msg };
    }
}
