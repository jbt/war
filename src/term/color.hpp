#pragma once

#include <cassert>
#include <cstdint>
#include <string_view>

namespace term {
    
    /**
     * @brief A color that is representable on a fancy terminal emulator
     * @todo Members indicating mode flags like bold, blink, itallic, whatever
     */
    class Color
    {
    public:
        using Channel = std::uint_least8_t; ///< The channels of sRGB have 8 bits and no negative values
        Channel r = 0; ///< red   value
        Channel g = 0; ///< blue  value
        Channel b = 0; ///< green value
        
        Color() = default; ///< See above default values
        Color(Color const&) = default; ///< copy construction should be permitted
        
        /**
         * @brief   Construct the most obvious interpretation of these red, green, and blue values
         * @details All other values are left to default.
         *          Note that this is an implicit constructor, on purpose.
         * @note    Anything convertible to Channel accepted as parameters.
         * @pre     rr, gg, and bb must be in-range, that is [0,255]
         * @param rr The value of the red   channel of the sRGB color
         * @param gg The value of the green channel of the sRGB color
         * @param bb The value of the blue  channel of the sRGB color
         */
        constexpr Color(auto rr,auto gg,auto bb)
        : r{ static_cast<Channel>(rr) }
        , g{ static_cast<Channel>(gg) }
        , b{ static_cast<Channel>(bb) }
        {
            assert( rr >= 0  );
            assert( rr <= 255);
            assert( gg >= 0  );
            assert( gg <= 255);
            assert( bb >= 0  );
            assert( bb <= 255);
        }
        
        explicit constexpr Color( auto rgb )
        : Color( rgb >> 16, (rgb >> 8) & 0xFF, rgb & 0xFF )
        {}

        /**
         * @brief   define a sort order
         * @details dark to light, significance: r > g > b
         * @param   rhs The right-hand side of the operator
         * @return  true iff it's a 'lesser' color
         */
        constexpr bool operator<( Color rhs ) const
        {
            #define COMPARE_MEMBER(X) if ( this-> X != rhs. X ) { return this-> X < rhs. X ; }
                COMPARE_MEMBER(r);
                COMPARE_MEMBER(g);
                COMPARE_MEMBER(b);
            #undef COMPARE_MEMBER
            return false;
        }
        
        /**
         * @brief Check for strict equality
         * @param c The color to check against (the RHS)
         * @return Return true iff the values of all channels are equal
         */
        constexpr bool operator==( Color c ) const
        {
            return ! ( *this < c || c < *this ) ;
        }

        /**
         * @brief Convert to an integer value
         * @return 8-bit unsigned channels, r most-significant, b least
         */
        constexpr auto rgb_code() const {
            return ( r << 16 ) | ( g << 8 ) | ( b );
        }
        
    };

    constexpr Color red  = { 255,   0,   0 };///< Very red
    constexpr Color black= {   0,   0,   0 };///< True black
    constexpr Color white= { 255, 255, 255 };///< maximal white
    
}
