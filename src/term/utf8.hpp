#pragma once

#include <string_view>

namespace term {

    /**
     * @brief Guess at how wide this will be on the screen
     * @param sv A view into the text to analyze
     * @todo  This implementation is shockingly wrong.
     *          @verbatim
     *          - f0 9f 99 bb is not 4 columns wide. It might be 1.
     *          - "One\nTwo" is 3 columns wide, not 7
     *          - \e[blah;blah might be negative width and should probably be disallowed elsewhere.
     *          @endverbatim
     * @return A column count
     */
    constexpr auto approx_text_width( std::string_view sv )
    {
        return static_cast<short>( sv.size() );
    }
    
}
