#pragma once

namespace term {

    bool signalled();
    void disregard_previous_signal();
    void throw_if_signalled();
    
}
