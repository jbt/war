#pragma once

#include <term/output.hpp>
#include <iosfwd>

namespace term  {
    
/**
 * @brief namespace for implementations of term::Output
 */
namespace output{
   
    /**
     * @brief possibly-transitional implementation based on the pre-existing code that was ad-hoc sending things to std::cout all over the place
     */
    class IOStream : public Output
    {
        std::ostream& under_;
    public:
        
        /**
         * @brief explicit because if you're doing this you really should know that's what you're doing.
         * @param underlying The stream the output, including ANSI escape sequences, is actually being sent to
         */
        explicit IOStream( std::ostream& underlying );
        ~IOStream() override;
        
        //For overridden methods, see documentation in base class

        short get_width() const override;
        
        IOStream& write( std::string_view ) override;
        IOStream&    set_color( Color const & ) override;
        IOStream&    set_bg_color( Color const & ) override;
        IOStream& set_position(Position) override;
        IOStream& clear_screen() override;
        IOStream&   reset_mode() override;
        
        void flush() const override;
    };
    
}}
