#include "iostream.hpp"
#include <term/color.hpp>
#include <term/signalled.hpp>
#include <cerrno>
#include <cstdio>
#include <cstring>
#include <iostream>

using O = term::output::IOStream;
using namespace std::literals;

namespace {
    constexpr auto csi = "\033["sv; // https://en.wikipedia.org/wiki/ANSI_escape_code#CSI_sequences
}

O::IOStream( std::ostream& underlying )
: under_{ underlying }
{
    under_ << csi << "?25l";
}
O::~IOStream()
{
    under_ 
        << csi << "0m"sv //reset the font mode
        << csi << "?25h" //unhide the cursor
        ;
}


auto O::write( std::string_view text ) -> O&
{
    throw_if_signalled();
    under_ << text;
    return *this;
}

auto O::clear_screen() -> O&
{
    throw_if_signalled();
    under_ << csi << "2J";
    return *this;
}

auto O::set_color( Color const& c ) -> O&
{
    throw_if_signalled();
    under_ << csi
           << '3'
           << "8;2"
           << ';' << static_cast<unsigned short>(c.r)
           << ';' << static_cast<unsigned short>(c.g)
           << ';' << static_cast<unsigned short>(c.b)
           << 'm';
    return *this;
}
auto O::set_bg_color( Color const& c ) -> O&
{
    throw_if_signalled();
    under_ << csi
           << '4'
           << "8;2"
           << ';' << static_cast<unsigned short>(c.r)
           << ';' << static_cast<unsigned short>(c.g)
           << ';' << static_cast<unsigned short>(c.b)
           << 'm';
    return *this;
}
auto O::reset_mode() -> O&
{
    under_ << csi << "0m"sv;
    return *this;
}
auto O::set_position( Position p ) -> O&
{
    throw_if_signalled();
    under_ << csi << p.row() << ';' << p.col() << 'H';
    return *this;
}

void O::flush() const
{
    under_.flush();
}

/*
 * TODO
 * If this class is truly using an IOStream as output, then it's inappropriate to try to access a TTY for stdout
 */
#if __has_include(<sys/ioctl.h>)

    #include <sys/ioctl.h>
    
    short O::get_width() const
    {
        ::winsize w = {};
        for ( int file_descriptor : {fileno(stdout), fileno(stderr), fileno(stdin)} )
        {
            if ( ::ioctl(file_descriptor, TIOCGWINSZ, &w) < 0 )
            {
//                 std::clog << std::strerror(errno) << std::endl;
            }
            else
            {
                return static_cast<short>( w.ws_col );
            }
        }
        return 80;
    }
    
#elif defined(_WIN32)

    #define WIN32_LEAN_AND_MEAN
    #define VC_EXTRALEAN
    #include <Windows.h>
    
    short O::get_width() const
    {
        CONSOLE_SCREEN_BUFFER_INFO csbi;
        ::GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
        return static_cast<short>( csbi.dwSize.X );
    }

#else

    #error "Implement term::output::IOstream::get_width() for your system."

    short O::get_width() const
    {
        return 80;
    }
    
#endif
