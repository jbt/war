#pragma once

#include <term/position.hpp>
#include <string_view>

/**
 * @brief A namespace for code related to interacting with a terminal emulator
 */
namespace term {
    
    class Color;

    /**
     * @brief   base class for different implementations of sinks - things that can handle output intended for a fancy terminal
     * @details Using dynamic polymorphism because:
     *          - Terminal rendering is slow anyhow. The time to dispatch on a vtable will be basically free by comparison.
     *          - I don't want to require the use of modules or leaking of library-specific details into every TU everywhere
     *          - It's at least conceivable someone could go full dynamic and add implementations to an already-built executable by way of dlopen etc
     */
    class Output
    {
    public:
        
        /**
         * @brief Simple dtor for a base class with no data members
         */
        virtual ~Output() = default;
        
        /**
         * @brief  Write some text at the cursor
         * @return self
         */
        virtual Output& write( std::string_view ) = 0;
        
        /**
         * @brief Clear all visible output, set cursor to position 1,1
         * @note  Does not change mode including color - that state is maintained
         */
        virtual Output& clear_screen() = 0;
        
        /**
         * @brief Ask the terminal how wide it currently is.
         * @note For some implementations this will be slow and return different answers as the terminal itself changes size.
         * @return The count of columns. Should never be negative. Makes a pretty sane assumption about the maximum terminal size.
         */
        virtual short get_width() const = 0;
        
        /**
         * @brief Set the foreground color to be used for coming blocks of text.
         * @return self
         */
        virtual Output& set_color( Color const& ) = 0;
        
        /**
         * @brief Set the background color to be used for coming blocks of text.
         * @return self
         */
        virtual Output& set_bg_color( Color const& ) = 0;
        
        /**
         * @brief Reset the 'mode' (color, style, etc.) of upcoming text back to default
         * @return self
         */
        virtual Output& reset_mode() = 0;

        /**
         * @brief Repositions the cursor
         * @return self
         */
        virtual Output& set_position( Position ) = 0;
        
        /**
         * @brief Indicate that a complete update has completed and should be written with human visible elements updated
         */
        virtual void flush() const = 0;
    };
    
}
