#!/usr/bin/env python3

from os import path
import subprocess
import sys
import time

release = '--release' in sys.argv
proj_dir = path.join(path.dirname(__file__), '..')

tag_list_output = subprocess.check_output( ['git', '-C', proj_dir, 'for-each-ref', 'refs/tags/'] )
tag_list_lines = tag_list_output.decode("utf-8").strip().split("\n")

def parse_tag( line ):
    return [ line.split(' ')[0], line.split('refs/tags/')[-1] ]

tags = dict( map( parse_tag, tag_list_lines ) )

commit_list_output = subprocess.check_output( ['git', '-C', proj_dir, 'log', '--format=%H %P', 'HEAD'] )
commit_list_lines = commit_list_output.decode("utf-8").strip().split("\n")
def parse_commit( line ):
    hashes = line.split(' ')
    return [ hashes[0], hashes[1:] ]

head = parse_commit( commit_list_lines[0] )
commits = dict( map( parse_commit, commit_list_lines ) )

version = ''

def parent_tags( commit ):
    if commit[0] in tags:
        return [ tags[commit[0]] ]
    result = []
    for parent in commit[1]:
        result += parent_tags( [ parent, commits.get(parent) ] )
    return sorted( result ) # TODO do a version sort not a lexicographical string sort

if head[0] in tags:
    version = tags[head[0]] 
    if release:
        print('error: CANNOT RELEASE ON TOP OF AN EXISTING RELEASE (', version, ')' )
        sys.exit( 1 )
else:
    recent_tags = parent_tags(head)
    prev_version = recent_tags[-1]
    version_components = prev_version.split('.')
    minor_build = int(version_components[-1]) + 1
    version = '.'.join( version_components[0:-1] + [ str(minor_build) ] )
    if not release:
        version += '.' + str(int(time.time()))

status_output = subprocess.check_output( ['git', '-C', proj_dir, 'status', '--porcelain'] )
status_lines = status_output.decode("utf-8").strip().split("\n")
if '' in status_lines:
    status_lines.remove('')

if len(status_lines) > 0:
    if release:
        print('error: YOUR WORKING DIRECTORY IS DIRTY:', ' '.join(status_lines) )
        sys.exit( 2 )
    #version += '.dirty'

print( version )
