#!/usr/bin/env python3

from os import path
import subprocess

output = subprocess.check_output(['g++', '--help=warnings']).decode('utf-8')
global lines
lines = output.split("\n")

lines = map(lambda l: l.strip(), lines)

lines = list(filter(lambda l: l.startswith('-W'), lines ))
for bad in ['=', '<', 'system headers', 'compat', 'Removed', 'eprecated']:
    lines[:] = list(filter(lambda l: not (bad in l), lines ))

opts = map(lambda l:l.split()[0], lines )

def is_cxx( opt ):
    empty_src = path.join(path.dirname(__file__), 'empty.cpp')
    cpp_out = subprocess.check_output(['g++', opt, '-xc++', '-E', empty_src], stderr=subprocess.STDOUT).decode('utf-8')
    return not( 'not for C++' in cpp_out )

opts = filter(is_cxx, opts)

print( ";".join( list(opts) ) )
