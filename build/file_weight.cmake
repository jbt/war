add_custom_target(weight
    COMMENT "Checking for files that should be decomposed..."
    COMMAND ${CMAKE_CURRENT_LIST_DIR}/file_weight.py
)
