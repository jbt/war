#!/usr/bin/env python3

import json
import subprocess
from os import path
from sys import argv

with open('compile_commands.json') as f:
    compile_commands = json.load(f)

warns = []

for tu in compile_commands:
    if '/test/' in tu['file']:
        continue
    cmd = tu['command'] + ' ' + ' '.join( argv[1:])
    if 'CMakeFiles/war.dir/' in cmd:
        #print( tu['file'] )
        output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT).decode('utf-8').strip()
        for line in output.split("\n"):
            try:
                warns += json.loads(line)
                #print( str(len(warns))+'...' )
            except json.JSONDecodeError:
                print( output ) #This will just happen sometimes, this is not an error.

print(str(len(warns))+' warnings found.')

with open('compiler.warnings.json', 'w') as f:
    f.write( json.dumps(warns, sort_keys=True, indent=2) )

