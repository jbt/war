
from os import path
import json

class Thresh:
    
    def __init__(self, slug):
        self.slug = slug
        self.storage_path = path.join( path.dirname(__file__), '..', 'res', slug + '.thresh.json' )
        with open(self.storage_path) as f:
            self.data = json.load( f )

    def __save(self):
        with open(self.storage_path, 'w') as f:
            f.write( json.dumps(self.data, sort_keys=True, indent=2) )
    
    def classify(self, value):
        return self.__classify(value, 0)
    
    def ratio(self):
        if not 'ratio' in self.data:
            self.data['ratio'] = 2
        return self.data['ratio']

    def count(self, idx):
        c = self.__cat( idx )
        if not 'count' in c:
            c['count'] = 0
        return c['count']
    
    def category_count(self):
        return len(self.data['cats'])

    def __cat(self, idx):
        cats = self.data['cats']
        if idx >= self.category_count():
            cats.append( {'name': str(self.category_count()), 'thresh': 1e308 } )
        return cats[idx]

    def __classify(self, value, idx):
        cat = self.__cat( idx )
        thresh = cat['thresh']
        #print( "classify: {} {} < {} {} = {} from {}".format(type(value),value,type(thresh),thresh,(value<thresh),cat) )
        if value < thresh:
            self.__hit( idx, value )
            return cat
        else:
            return self.__classify(value, idx + 1)

    def __hit(self, idx, val):
        cat = self.__cat( idx )
        if idx > 0 and self.count(idx-1) < self.count(idx) * self.ratio():
            c = self.__cat( idx - 1 )
            m = self.count(idx-1) + self.count(idx)
            t = c['thresh']
            c['thresh'] = ( t * m + val ) / ( m + 0.99 )
            #print( "Raising {} from {} to {} because {} was hit".format(c['name'],t,c['thresh'],cat['name']) )
        if idx + 1 < self.category_count() and self.count(idx)  > self.count(idx+1) * self.ratio():
            m = self.count(idx) + self.count(idx+1)
            t = cat['thresh']
            cat['thresh'] = ( t * m + val ) / ( m + 1.01 )
            #print( "Lowering {} ({}) from {} to {} because of neighbor {} ({})".format(cat['name'],self.count(idx),t,cat['thresh'],self.__cat(idx+1)['name'],self.count(idx+1)) )
        cat['count'] = self.count( idx ) + 1
        self.__save()
