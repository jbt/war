#!/usr/bin/env python3

from os import path
import json
import itertools
import pathlib 
#from setuptools.namespaces import flatten
from shutil import copyfile
import sys

priorities = [
    
    #Coalate high-value warnings of various types
    'lcov-superlarge-file',
    '-Wsign-conversion',
    'dep-up',
    'file-weight-superlarge',
    
    'lcov-verylarge-file',
    '-Wmissing-braces',
    'file-weight-verylarge',
    
    'lcov-medium-file',
    '-Waggregate-return',
    'file-weight-modest',
    
    'lcov-modest-file',

    #TODO these are sorted just by name
    'clang-diagnostic-padded',
    'clang-diagnostic-sign-conversion',
    'cppcoreguidelines-avoid-magic-numbers',
    'cppcoreguidelines-init-variables',
    'cppcoreguidelines-macro-usage',
    'cppcoreguidelines-pro-bounds-array-to-pointer-decay',
    'cppcoreguidelines-pro-bounds-constant-array-index',
    'cppcoreguidelines-pro-type-vararg',
    'cppcoreguidelines-special-member-functions',
    'doxy-undoc-enum',
    'doxy-undoc-function',
    'doxy-undoc-method',
    'doxy-undoc-return',
    'file-weight-large',
    'file-weight-medium',
    'file-weight-modest',
    'file-weight-small',
    'file-weight-superlarge',
    'file-weight-verylarge',
    'fuchsia-default-arguments-calls',
    'fuchsia-default-arguments-declarations',
    'fuchsia-overloaded-operator',
    'fuchsia-trailing-return',
    'google-explicit-constructor',
    'google-readability-namespace-comments',
    'google-runtime-int',
    'google-runtime-references',
    'hicpp-explicit-conversions',
    'hicpp-no-array-decay',
    'hicpp-signed-bitwise',
    'hicpp-special-member-functions',
    'hicpp-uppercase-literal-suffix',
    'hicpp-vararg',
    'lcov-function',
    'lcov-line',
    'lcov-modest-file',
    'lcov-small-file',
    'llvm-header-guard',
    'llvm-include-order',
    'llvm-namespace-comment',
    'misc-non-private-member-variables-in-classes',
    'modernize-use-nodiscard',
    'modernize-use-trailing-return-type',
    'readability-container-size-empty',
    'readability-else-after-return',
    'readability-implicit-bool-conversion',
    'readability-magic-numbers',
    'readability-named-parameter',
    'readability-uppercase-literal-suffix',
    '-Waggregate-return',
    '-Wmissing-braces',
    '-Wpadded',
    '-Wsign-conversion',
    
    # Some warnings I actively don't want to fix:
    '-Wattributes', # This is mainly an issue with me using an attribute from a newer toolchain, and then building on an older one
    'bugprone-branch-clone' # I like the construct of switch and return true on some cases and false on others.
]

warnings = {}

for path in pathlib.Path('.').rglob('*.warnings.json'):
    with open(path) as f:
        print("Reading in "+str(path))
        try:
            warnings[path.stem] = json.load( f )
        except json.decoder.JSONDecodeError:
            print(str(path)+" is not valid JSON!")
            exit(1)


def loc_str(diag):
    if not 'locations' in diag:
        return ''
    for loc in diag['locations']:
        for l in loc.values():
            if 'file' in l and 'line' in l:
                return l['file'] + ':' + str(l['line'])

def category(diag):
    if 'category' in diag:
        return diag['category']
    elif 'option' in diag:
        return diag['option']
    else:
        return 'unspecified'

def disp_str(diag):
    return "{0:<25}|{1:<25}|{2}".format( category(diag)[-25:], loc_str(diag).split('/war/')[-1], diag['message'][:80] )

def score_diag(diag):
    if diag['kind'] != 'warning':
        #print("Ignoring non-warning:"+str(diag))
        return 0
    if 'category' in diag:
        cat = diag['category']
    elif 'option' in diag:
        cat = diag['option']
    else:
        #print("Ignoring warning of unknown category:"+str(diag))
        return 0
    try:
        prio = len(priorities) - priorities.index( cat )
        return prio
    except ValueError:
        print("Add warning type '" + cat + "' to the list of priorities in " + __file__ + " . Example: " + diag['message'] + ' @ ' + str(diag['locations']) )
        exit( 1 )

def score_list(diag_list):
    score_sum = 0
    for entry in diag_list:
        #print(type(entry))
        if type(entry) == list:
            score_sum += score_list(entry)
        elif type(entry) == dict:
            score_sum += score_diag(entry)
        else:
            print("Improper entry here: " + str(entry))
            exit(2)
    return score_sum

def for_display(obj):
    if type(obj) == list:
        rv = {}
        for o in obj:
            rv = dict( for_display(o), **rv )
        return rv
    if not type(obj) == dict:
        return {}
    if 'kind' in obj and obj['kind'] == 'warning':
        return { category(obj): (score_diag(obj), disp_str(obj)) }
    return for_display( list(obj.values()) )

prev = {}
with open(sys.argv[1]) as inf:
    prev = json.load( inf )

for k in prev.keys():
    if not k in warnings:
        warnings[k] = prev[k]
    elif not k in prev:
        print("Warning group {} appears new.".format(k))
    elif len(warnings[k]) > len(prev[k]):
        print( "{}: {} > {} -> {} >? {}".format(k,len(warnings[k]),len(prev[k]),score_list(warnings[k]),score_list(prev[k])) )

with open('warnings_collected.json', 'w') as out:
    out.write( json.dumps(warnings, sort_keys=True, indent=2) )

lines = list( for_display(warnings).values() )
lines.sort()
print("Worst warnings:")
for line in lines[-20:]:
    print(line[1])

result      = score_list(list(warnings.values()))
prev_result = score_list(list(    prev.values()))

print( "score:", result )
print( "previous score:", prev_result )

if    result >  prev_result:
    print('Fix some warnings!')
    exit( 3 )
elif result == prev_result:
    print('No change in warnings one way or another.')
else:
    print('Congrats on reducing warnings. You have raised the bar.')
    copyfile('warnings_collected.json', sys.argv[1])
