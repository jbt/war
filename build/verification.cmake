
file(
    GLOB_RECURSE
    all_headers
    src/*.hpp
)

foreach(header ${all_headers})
    get_filename_component(dir "${header}" DIRECTORY)
    get_filename_component(stem "${header}" NAME_WE)
    set(corresponding_source "${dir}/${stem}.cpp")
    if(EXISTS "${corresponding_source}")
        message(VERBOSE " ${corresponding_source} is pairted with ${header}")
    else()
        message(FATAL_ERROR "For each header like ${header} there must be a corresponding source file like ${corresponding_source} .")
    endif()
    file(READ "${corresponding_source}" corresponding_source_contents)
    set(output_variable unchanged)
    string(REGEX MATCH
        "^[ \t]*#include[ \t]*\"${stem}.hpp\""
        include_directive
        "${corresponding_source_contents}"
    )
    if(include_directive)
        message(VERBOSE " Found '${include_directive}'")
    else()
        message(FATAL_ERROR "${corresponding_source} did not include its own header in double-quotes like #include \"${stem}.hpp\".")
    endif()
endforeach()

add_custom_target(ce_cov
    COMMAND python3 "${CMAKE_CURRENT_LIST_DIR}/constexpr_coverage.py" #TODO use proper idiomatic portable cmake for python access
    WORKING_DIR "${CMAKE_CURRENT_BINARY_DIR}"
)

include(${CMAKE_CURRENT_LIST_DIR}/lcov.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/file_weight.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/dependency_checks.cmake)

add_custom_target(warn
    DEPENDS 
        compiler.warnings.json 
        cov
        doc
        weight
        dep_checks
    COMMAND python3 ${CMAKE_CURRENT_LIST_DIR}/warning_score.py "${CMAKE_CURRENT_SOURCE_DIR}/res/warnings.json" 
)

find_program(CLANG_TIDY_CMD clang-tidy)
if(CLANG_TIDY_CMD)
    add_custom_target(
        clang_tidy
        COMMENT "Running clang-tidy static analysis"
        COMMAND python3 ${CMAKE_CURRENT_LIST_DIR}/clang_tidy.py "${CLANG_TIDY_CMD}" 
        BYPRODUCTS clang_tidy.warnings.json
    )
    add_dependencies(warn clang_tidy)
endif()

add_custom_target(verify
    DEPENDS ce_cov
    DEPENDS warn
)
