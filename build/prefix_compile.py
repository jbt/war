#!/usr/bin/env python3

from os  import path
import json
import subprocess
import sys
import time

with open('compile.log', 'a') as log:
    log.write( str(time.time()) + ' ' )
    log.write( ' '.join(sys.argv) + "\n" )

cmd = ' '.join( sys.argv[1:] )

for arg in sys.argv[2:] :
    if arg.endswith('.o') and path.isdir(path.dirname(arg)):
        exit_code = 1
        with open(arg + '.compiler_output', 'w') as log:
            exit_code = subprocess.call(sys.argv[1:], stderr=log)
        if exit_code != 0:
            with open(arg + '.compiler_output') as log:
                s = log.read()
                print( s, file=sys.stderr )
                try:
                    j = json.loads( s.strip() )
                    reported_locs = {}
                    for m in j:
                        if not 'kind' in m:
                            print('What is this? ' + str(m))
                        if 'error' == m['kind']:
                            needs_msg = True
                            for loc in m['locations']:
                                f = loc['caret']['file']
                                l = loc['caret']['line']
                                if f in reported_locs:
                                    if reported_locs[f] < l:
                                        continue
                                reported_locs[f] = l
                                if needs_msg:
                                    print( m['message'], file=sys.stderr )
                                    needs_msg = False
                                print( f + ' ' + str(l), file=sys.stderr )
                except json.decoder.JSONDecodeError:
                    print('output is not json', file=sys.stderr)
        exit( exit_code )

print("error: Did not find the object file mentioned in command '"+cmd+"', please edit "+__file__+" to be able to find your system's style of object file paths.")
exit(1)
