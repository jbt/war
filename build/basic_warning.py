
def create( category, message, source_file, line = 1, column = 1 ):
    return {
        'children' : [],
        'kind'     : 'warning',
        'locations': [
            {
                "caret": {
                    "column": column,
                    "file"  : source_file,
                    "line"  : line
                }
            }
        ],
        'message'  : message,
        'option'   : category,
        'category' : category
    }
