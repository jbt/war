
find_program(LCOV_CMD lcov)

if(NOT "${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
    add_custom_target(cov
        COMMENT "Coverage reports are not run on optimized code because they are misleading."
        DEPENDS run_ut
    )
elseif(LCOV_CMD)
    find_program(GENHTML_CMD genhtml)
    find_program(DEMANGLE_CMD c++filt)
    if(DEMANGLE_CMD)
        set(demangle "--demangle-cpp")
    else()
        set(demangle "")
    endif()
    # TODO support other kinds of coverage reports
    add_custom_target(cov
        COMMENT "Generating coverage report..."
        DEPENDS run_ut
        COMMAND ${CMAKE_COMMAND} -E remove lcov.info
        COMMAND ${CMAKE_COMMAND} -E remove_directory cov/
        COMMAND "${LCOV_CMD}" --capture --directory . --output lcov.info --base-directory "${CMAKE_CURRENT_SOURCE_DIR}/src/" --no-external 
        COMMAND ${CMAKE_CURRENT_LIST_DIR}/lcov_warnings.py lcov.info ${srcs} ${all_headers}
        COMMAND "${GENHTML_CMD}" --output-directory cov/ --show-details --legend ${demangle} --missed lcov.info 
    )
else()
    message(WARNING "TODO: implement test coverage for your platform. Or install the proper tool.")
    add_custom_target(cov
        COMMENT "Coverage reports cannot be run because it's not set up."
        COMMAND false
    )
endif()
