
add_custom_target(dep_checks
    COMMENT "Checking to see if new versions of libraries we're using are available."
    COMMAND ${CMAKE_CURRENT_LIST_DIR}/dependency_updates.py 
)
