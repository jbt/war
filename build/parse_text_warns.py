
import basic_warning
import json
import re
import sys

warn_line_re = '^([^:]+):(\d+):(\d+:)?\s*warning:\s*(.*?)(\[[^\]]+\])?$'

common_cat_patterns = [
    [           'parameters of.*are not.*documented', 'doxy-undoc-params'  ],
    [            'return type of.*is.not.documented', 'doxy-undoc-return'  ],
    ['Member.*function.*of class.*is not documented', 'doxy-undoc-method'  ],
    ['Member.*function.*namespace.*s not documented', 'doxy-undoc-function'],
    ['Member.*enumerat.*namespace.*s not documented', 'doxy-undoc-enum'    ],
]

def from_line( line, default_category ):
    m = re.search( warn_line_re, line )
    if m:
        fi = m[1]
        li = int(m[2])
        if m[3]:
            co = int(m[3][0:-1])
        else:
            co = 1
        me = m[4]
        if m[5]:
            ca = m[5][1:-1]
        else:
            ca = default_category
            for (pat,cat) in common_cat_patterns:
                if re.match(pat, me):
                    ca = cat
                    break
        return basic_warning.create( ca, me, fi, li, co )

def from_string( s, default_category ):
    as_warns = map( lambda l: from_line(l, default_category), s.split("\n") )
    warns_only = filter( lambda w:not w is None , as_warns )
    return list(warns_only)

if __name__ == '__main__':
    for f in sys.argv[1:]:
        warns = from_string( open(f).read(), f.split('.')[0] )
        if f.endswith('.txt'):
            out = f[:-4] + '.json'
        else:
            out = f + '.json'
        open( out, 'w' ).write( json.dumps(warns, sort_keys=True, indent=2) )
