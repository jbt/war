find_package(Doxygen)

if(DOXYGEN_FOUND)

    set(DOXYGEN_IN ${CMAKE_CURRENT_SOURCE_DIR}/docs/Doxyfile.in)
    set(DOXYGEN_OUT ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile)
    configure_file(
        ${DOXYGEN_IN}
        ${DOXYGEN_OUT}
        @ONLY
    )

    add_custom_target(
        doc
        COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
        COMMAND python3 ${CMAKE_CURRENT_LIST_DIR}/parse_text_warns.py doxygen.warnings.txt
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "Generating documentation with Doxygen"
    )

else()

  message(WARNING "Doxygen need to be installed to generate the doxygen documentation")
  
endif()
