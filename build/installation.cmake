include(GNUInstallDirs)

install(
    TARGETS
        war
    RUNTIME DESTINATION
        bin
)
install(
    FILES
        res/logo.png
    DESTINATION
        "${CMAKE_INSTALL_DATAROOTDIR}/icons/"
    RENAME
        war.png
)
configure_file(
    res/war.desktop.in
    "${CMAKE_CURRENT_BINARY_DIR}/war.desktop"
    @ONLY
    NEWLINE_STYLE 
        UNIX
)
install(
    FILES
        "${CMAKE_CURRENT_BINARY_DIR}/war.desktop"
    DESTINATION
        "${CMAKE_INSTALL_DATAROOTDIR}/applications/"
)

set(CPACK_PACKAGE_DESCRIPTION "You've seen card games emulated in software. Normally it handles all the tedium (shuffling, etc.) and only stops when the player needs to make a decision. What if there were no decisions to make? What if the game were just tedium for its own sake?")
set(CPACK_PACKAGE_ICON res/logo.png)
set(CPACK_PACKAGE_VENDOR John)
set(CPACK_PACKAGE_CONTACT ${CPACK_PACKAGE_VENDOR})
set(CPACK_PACKAGE_CHECKSUM SHA512)


function(package_format_if_available format_token test_command)
    set(format ${format_token})
    list(POP_FRONT ARGV)
    execute_process(
        COMMAND ${ARGV}
        RESULT_VARIABLE test_command_result
        OUTPUT_QUIET
    )
    if(${test_command_result} EQUAL 0)
        list(APPEND CPACK_GENERATOR ${format})
        set(CPACK_GENERATOR
            "${CPACK_GENERATOR}"
            PARENT_SCOPE
        )
        set(CPACK_BINARY_${format} ON CACHE BOOL "Enable to build ${format} packages" FORCE)
    else()
        message(DEBUG "Will not generate a package of type ${format} because command ${ARGV} returned ${test_command_result}")
        set(CPACK_BINARY_${format} OFF CACHE BOOL "Enable to build ${format} packages" FORCE)
    endif()
endfunction()

# Normal package managers

package_format_if_available(DEB dpkg     --version)
package_format_if_available(RPM rpmbuild --version)

# Archives
list(APPEND CPACK_GENERATOR 7Z  )
list(APPEND CPACK_GENERATOR STGZ)
list(APPEND CPACK_GENERATOR TGZ )
list(APPEND CPACK_GENERATOR TXZ )
list(APPEND CPACK_GENERATOR ZIP )

#Exotic stuff
list(APPEND CPACK_GENERATOR External)
package_format_if_available(IFW binarycreator --help)

include(CPack)
