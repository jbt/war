#!/usr/bin/env python3

import basic_warning
import json
import subprocess

info_json = 'conan.info.json'

subprocess.run( ['conan', 'info', '--update', '--json', info_json, '.'], check=True )

with open(info_json) as f:
    packages = json.load( f )

def is_direct_dep( p ):
    return 'required_by' in p and 'conanfile.txt' in p['required_by']

my_direct_deps = filter( is_direct_dep, packages )

def has_update( p ):
    return 'pdate' in p['recipe']

with_ups = filter( has_update, my_direct_deps )

def warn( p ):
    if 'provides' in p:
        name = p['provides'][0]
    else:
        name = p['reference'].split('/')[0]
    return basic_warning.create( 'dep-up', "You are using an out-of-date version of {} .".format(name), 'CMakeLists.txt' )

result = list( map( warn, with_ups ) )

with open('dependency_updates.warnings.json', 'w') as out:
    out.write( json.dumps(result, sort_keys=True, indent=2) )
