if(WARNINGS)
     execute_process(
         COMMAND python3 "${CMAKE_CURRENT_LIST_DIR}/gcc_documented_W_options.py"
         OUTPUT_VARIABLE compiler_warning_flags
         OUTPUT_STRIP_TRAILING_WHITESPACE
     )
     list(
        REMOVE_ITEM
        compiler_warning_flags
        -Wabi 
        -Wsystem-headers
        -Wtemplates
        -Wnamespaces
     )
     list(
        APPEND
        compiler_warning_flags
        -Werror=all -Werror=return-type # -fdiagnostics-format=json 
     )
else()
    set(compiler_warning_flags -Wall;-Wextra;-Wpedantic;-Werror=all;-Werror=return-type)
endif()

set(CMAKE_CXX_FLAGS_DEBUG   " -g3 -ggdb3 -O0 -UNDEBUG " )
set(CMAKE_CXX_FLAGS_RELEASE " -g0 -ggdb0 -Ofast -march=native -mtune=native -DNDEBUG=1 " ) #TODO arch=native is a bad idea for where we're going
set(force_include_opt -include )

if("${CMAKE_BUILD_TYPE}" STREQUAL Debug)
    set(coverage_opt --coverage )
    set(coverage_lib gcov )
else()
    message(WARNING "Will not instrument for code coverage in optimized code (CMAKE_BUILD_TYPE==${CMAKE_BUILD_TYPE})")
endif()

add_custom_command(
    OUTPUT compiler.warnings.json
    COMMENT "Recompiling to collect compiler warnings in JSON format."
    COMMAND python3 "${CMAKE_CURRENT_LIST_DIR}/recompile_all.py" -fdiagnostics-format=json 
)

set(static_link_opt -static;-static-libstdc++)
