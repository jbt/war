#!/usr/bin/env python3

from os import path
import json
import subprocess
import sys

import file_weight

lines = open(sys.argv[1]).readlines()

current_file = 'unknown'

fooline = {}

covered_files = []
have_filt = 0 == subprocess.call( ['c++filt', '--version'], stdout = subprocess.DEVNULL )
warnings = []

def issue_warning(source, line, message, category):
    cat = 'lcov-' + category
    warnings.append( {
            "category" : cat,
            "children" : [],
            "kind"     : "warning",
            "locations": [
                {
                    "caret": {
                        "column": 1,
                        "file"  : source,
                        "line"  : int(line)
                    }
                }
            ],
            "message": message,
            "option" : cat
        })

def noop_handler( data ):
    pass

def source_file_context( data ):
    global current_file
    current_file = data[0]
    covered_files.append( path.realpath(current_file) )

def function_declartion( data ):
    fooline[data[1]] = data[0]

def function_coverage( data ):
    if data[0] != '0':
        return
    symbol = data[1]
    name = symbol
    if have_filt:
        name = subprocess.check_output( ['c++filt', data[1]] ).decode('utf-8').strip()
    line = fooline[symbol]
    issue_warning( current_file, line, "Function '{}' is not called by a unit test".format(name), 'function' )

def line_coverage( data ):
    if data[1] != '0':
        return
    issue_warning( current_file, data[0], "Line of code not executed by a unit test", 'line' )

handlers = {
    'SF'  : source_file_context, #SF:<absolute path to the source file>
    'FN'  : function_declartion, #FN:<line number of function start>,<function name>
    'FNDA': function_coverage  , #FNDA:<execution count>,<function name>
    'DA'  : line_coverage      , #DA:<line number>,<execution count>[,<checksum>]
    
    'TN'  : noop_handler, # TN:<test name> ... always empty for us
    'FNF' : noop_handler, # FNF:<number of functions found>
    'FNH' : noop_handler, # FNH:<number of function hit>
    'BRF' : noop_handler, # BRF:<number of branches found>
    'BRH' : noop_handler, # BRH:<number of branches hit>
    
    #TODO maybe handle branch coverage someday
    'BRDA' : noop_handler # BRDA:<line number>,<block number>,<branch number>,<taken>
}

for line in lines:
    if ':' in line:
        colon = line.index(':')
        directive = line[:colon]
        data = line[(colon+1):].strip().split(',')
        handlers[directive]( data )

def might_be_code( line ):
    if 'static_assert' in line:
        return False
    return '}' in line and not '};' in line

uncovered_files = []
for source in sys.argv[2:]:
    if not path.realpath(source) in covered_files:
        with open(source) as sf:
            lines = sf.readlines()
        size = len(list(filter(might_be_code,lines)))
        uncovered_files.append( [size,source] )

uncovered_files.sort()

for entry in uncovered_files:
    size = entry[0]
    source = entry[1]
    (weight,size_cat) = file_weight.lookup(source)
    if size_cat != 'good':
        issue_warning( source, 1, "Source file with weight category ({}) is not covered by unit tests".format(size_cat), "{}-file".format(size_cat) )

with open('lcov.warnings.json', 'w') as output_file:
    output_file.write( json.dumps(warnings, sort_keys=True, indent=2) )
