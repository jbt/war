#!/usr/bin/env python3

import json
import os
import subprocess
import sys
import parse_text_warns

scratch_dir = 'ct'
if not os.path.isdir(scratch_dir):
    os.mkdir( scratch_dir )

compile_commands_file = 'compile_commands.json'
with open(compile_commands_file) as f:
    compile_commands = json.load( f )

for cc in compile_commands:
    cc['command'] = cc['command'].replace( '-fdiagnostics-format=json', '' )

with open(os.path.join(scratch_dir,compile_commands_file),'w') as altered_copy:
    altered_copy.write( json.dumps(compile_commands, sort_keys=True, indent=2) )

sources = list( filter( lambda f: not '/test/' in f,  map(lambda n: n['file'], compile_commands) ) )
sources += list( filter( lambda h: os.path.isfile(h), map( lambda c: c.replace('.cpp','.hpp'), sources ) ) )

#todo argv(1)
cmd = [ 'clang-tidy', '-p', scratch_dir, '--checks=*' ] + sources 
print( ' '.join(cmd) )
proc = subprocess.Popen( cmd, stdout=subprocess.PIPE )

sout, serr = proc.communicate(timeout=len(sources)*30)
ec = proc.returncode
out = sout.decode("utf-8")
#err = serr.decode("utf-8")

if ec != 0:
    print( "Command '"+(' '.join(cmd))+"' failed with exit code "+str(ec)+":\n" + out, file = sys.stderr )
    exit( ec )

#print("Captured stdout:" + out)
#print("Captured stderr:" + err)

warns = parse_text_warns.from_string( out, 'clang-tidy' )

json_text = json.dumps(warns, sort_keys=True, indent=2)
#print( json_text )

with open('clang_tidy.warnings.json', 'w') as out:
    out.write( json_text )
