#!/usr/bin/env python3

import json
from os import path
import os
import re
import shutil
import sys
import time

#TODO this script makes assumptions about object file naming AND the syntax of 2>/dev/null - make it more portable. There's no great reason this shouldn't be compilable for Windoze

obj_file_re = re.compile( r'[^\s]+\.o(bj)?' )
exit_code = 0

def evaluate( tu ):
    error_count = 0
    orig_cpp = tu['file']  #[u'directory', u'command', u'file']
    orig_hpp = orig_cpp.replace('.cpp','.hpp')
    if not path.isfile(orig_hpp):
        return 0
    scratch_hpp = 'ce_cov/' + path.basename(orig_hpp)
    scratch_cpp = 'ce_cov/scratch.cpp'
    scratch_o   = 'ce_cov/scratch.o'
    cert = scratch_hpp + '.cert'
    if path.isfile(cert):
        if   path.getmtime(cert) <= path.getmtime(orig_hpp):
            os.remove( cert )
        elif path.getmtime(cert) <= path.getmtime(orig_cpp):
            os.remove( cert )
        else:
            return 0
    cmd = tu['command'].replace( orig_cpp, scratch_cpp )
    cmd = obj_file_re.sub( scratch_o, cmd )
    shutil.copyfile( orig_cpp, scratch_cpp ) 
    code = open(orig_hpp).read().split('constexpr ')
    for i in range(1,len(code)):
        prefix = 'constexpr '.join( code[:i] )
        suffix = 'constexpr '.join( code[i:] )
        attempt_source = prefix + ' inline ' + suffix
        with open(scratch_hpp, 'w') as scratch:
            scratch.write( attempt_source )
            scratch.flush()
        ec = os.system( cmd + ' 2>/dev/null ' )
        if ec == 0:
            print( cmd )
            print( '{}:{}: error: source compiled even with the constexpr removed. Add a static_assert.'.format(orig_hpp, prefix.count('\n') + 1) )
            print( prefix.split('\n')[-1] + "\033[38;2;255;1;64;1;3;5m constexpr \033[0m" + suffix.split('\n')[0] )
            sys.exit( 1 )
            error_count += 1
        os.remove( scratch_hpp )
    with open(cert, 'w') as c:
        c.write(str(len(code)))
    return error_count

def process( compile_commands ):
    with open(compile_commands) as f:
        compilation_data = json.load(f)
    error_count = 0
    for translation_unit in compilation_data:
        error_count += evaluate( translation_unit )
    #print( '{} errors detected.'.format(error_count) )
    sys.exit( error_count )

if __name__ == '__main__':
    if not path.isdir('ce_cov'):
        os.mkdir('ce_cov')
    process( 'compile_commands.json' )
