#!/usr/bin/env python3

from os import path
import glob
import subprocess
import sys
import thresh
import time

now = time.time()
here = path.dirname(__file__)

def change_frequency( f ):
    output = subprocess.check_output( ['git', '-C', here, 'log', '--format=%at', f] ).decode('utf-8').strip()
    lines = filter( lambda l: len(l)>0, output.split("\n") )
    times = list( map( int, lines ) )
    times.sort()
    mod_count = len(times)
    if len(times) == 0:
        return 0
    age = ( now - times[0] )
    return  mod_count / age

def base_score(f):
    return ( path.getsize(f) * change_frequency(f), f )

def weighable(p):
    for undecomposible_suffix in ['.json', '.webm', '.pyc']:
        if p.endswith(undecomposible_suffix):
            return False
    return path.isfile(p)

paths = map( path.realpath, glob.glob(here+'/../**/*', recursive=True) )
files = list( filter(weighable, paths) )
scored= list( map(base_score, files) )

refcounts = dict( map( lambda f: (f,1.0), files ) )

def incref( p ):
    if not '.' in p:
        return
    for f in files:
        if f.endswith(p):
            refcounts[f] += 1

for c in files:
    try:
        with open(c) as f:
            lines = f.readlines()
        for line in lines:
            if '#include' in line:
                if '<' in line:
                    incref( line.split('<')[-1].split('>')[-2] )
                elif '"' in line:
                    incref( line.split('"')[-2] )
    except:
        pass

rescored = list( map(lambda s: (s[0]*refcounts[s[1]], s[1]), scored) )
rescored.sort()

severities = thresh.Thresh( 'file_weight' )

if __name__ == '__main__':
    jout = open( 'file_weight.warnings.json', 'w' )
    print( '[', file=jout )
    notfirst = False
    for (s,f) in rescored:
        try:
            severity = severities.classify(s)
        except IndexError as ie:
            print( "{} scored a {}, which is too much: {}".format(f,s,ie) )
            next
        if severity['name'] == 'good':
            continue
        if notfirst:
            print( ',', file=jout )
        else:
            notfirst = True
        cat = 'file-weight-' + severity['name']
        print( '  {', file=jout )
        print( '    "category": "{}",'.format(cat), file=jout )
        print( '    "children": [],', file=jout )
        print( '    "kind": "warning",', file=jout )
        print( '    "locations": [', file=jout )
        print( '      {', file=jout )
        print( '        "caret": {', file=jout )
        print( '          "column": 1,', file=jout )
        print( '          "file": "{}",'.format(f), file=jout )
        print( '          "line": 1', file=jout )
        print( '        }', file=jout )
        print( '      }', file=jout )
        print( '    ],', file=jout )
        print( '    "message": "See if this file can be decomposed. Weight={}",'.format(s), file=jout )
        print( '    "option": "{}"'.format(cat), file=jout )
        jout.write( '  }' )

    if notfirst:
        print( "\n]", file=jout )
    else:
        print( "]", file=jout )

realpath2score = dict(e[::-1] for e in rescored)

def lookup(f):
    score = realpath2score[path.realpath(f)]
    sc = severities.classify(score)
    #print("lookup({}): score={} , sc={}".format(f,score,sc))
    #print("{} : {} ... {}".format(f,score,sc) )
    return (score, sc['name'])
