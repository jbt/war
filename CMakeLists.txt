cmake_minimum_required(VERSION 3.13)

execute_process(
    COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/build/version.py
    OUTPUT_VARIABLE war_version
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

project(war
    VERSION ${war_version}
    DESCRIPTION "A card 'game' that asks very little of its 'players'"
    HOMEPAGE_URL "https://gitlab.com/jbt/war/"
    LANGUAGES CXX
)

set(WARNINGS OFF CACHE BOOL "Crank up the volume on compiler and compiler-like warnings.")

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

include("${CMAKE_CURRENT_SOURCE_DIR}/build/conan.cmake")
include(
    "${CMAKE_CURRENT_SOURCE_DIR}/build/${CMAKE_CXX_COMPILER_ID}.cmake"
    OPTIONAL
    RESULT_VARIABLE COMPILER_FLAGS
)
if(NOT COMPILER_FLAGS)
    message(
        WARNING
        "Please strongly consider creating the file ${CMAKE_CURRENT_SOURCE_DIR}/build/${CMAKE_CXX_COMPILER_ID}.cmake to set flags for your compiler."
    )
endif()

# if(WARNINGS)
#     set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "python3 ${CMAKE_CURRENT_SOURCE_DIR}/build/prefix_compile.py ${RULE_LAUNCH_COMPILE}")
# endif()

conan_cmake_run(
    REQUIRES
           magic_enum/0.6.6
          boost_beast/1.69.0@bincrafters/stable
boost_program_options/1.69.0@bincrafters/stable
               catch2/2.13.0
    BASIC_SETUP 
    CMAKE_TARGETS
    BUILD missing
)

file(
    GLOB_RECURSE
    srcs
    CONFIGURE_DEPENDS
    src/*.cpp
)

add_executable(war
    ${srcs}
)
add_executable(war_static
    EXCLUDE_FROM_ALL
    ${srcs}
)
foreach(b war;war_static)
    target_compile_features(${b}
        PRIVATE
            cxx_std_20
    )
    target_compile_definitions(${b}
        PRIVATE
            WAR_VERSION=\"${PROJECT_VERSION}\"
    )
    target_include_directories(${b}
        PRIVATE
            src
    )
    target_link_libraries(${b}
        PRIVATE
            CONAN_PKG::magic_enum
            CONAN_PKG::boost_beast
            CONAN_PKG::boost_program_options
    )
endforeach()

if(compiler_warning_flags)
    target_compile_options(war
        PRIVATE
            ${compiler_warning_flags}
    )
endif()

target_link_libraries(war_static
    PUBLIC
        ${static_link_opt}
)

add_subdirectory(test/unit)
include(build/doxygen.cmake)
include(build/installation.cmake)
include(build/verification.cmake)
